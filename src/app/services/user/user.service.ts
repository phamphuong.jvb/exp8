import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders ,HttpParams} from '@angular/common/http';
import { User } from '../../_models';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class UserService {
    apiURL = 'http://localhost:3000';
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
	constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
     }
	// Http Options

    getAll() {
        return this.http.get<User[]>(this.apiURL + `/login`);
    }

    getById(id: number) {
        return this.http.get(this.apiURL + `/login` + id);
    }

    register(user: User) {
        return this.http.post(this.apiURL + `/login`, user);
    }

    update(user: User) {
        return this.http.put(this.apiURL + `/login` + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(this.apiURL + `/login` + id);
	}
    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }   
	login(email: string, password: string) {
		const Options = {
			params : new  HttpParams({
                fromObject :{
                    email: email, 
                    password: password
                }
            }),
			headers: new HttpHeaders({
				'Content-Type': 'application/json'
			}),
		}
        return this.http.get<any>(this.apiURL + `/login`, Options)
            .pipe(map(user => {
                console.log(user)
                // login successful if there's a jwt token in the response
                if (user) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }

                return user;
            }));
    }
    // login(email: string, password: string) {
    //     return this.http.post<any>(this.apiURL + `/login`, { email: email, password: password })
    //         .pipe(map(user => {
    //             // login successful if there's a jwt token in the response
    //             if (user && user.token) {
    //                 // store user details and jwt token in local storage to keep user logged in between page refreshes
    //                 localStorage.setItem('currentUser', JSON.stringify(user));
    //             }

    //             return user;
    //         }));
    // }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
	// constructor() { }

	// doLogin(data){
	// 	if (data.email == "admin@gmail.com" && data.password == "admin123") {
	// 		return {
	// 			code : 200,
	// 			message : "Login Successful",
	// 			data : data
	// 		};
	// 	}else{
	// 		return {
	// 			code : 503,
	// 			message : "Invalid Credentials",
	// 			data : null
	// 		};
	// 	}
	// }

	// doRegister(data){
		// 	return this.http.post('user-add.php',data);	
		// }
	}