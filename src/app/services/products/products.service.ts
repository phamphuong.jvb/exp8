
 import { Injectable } from '@angular/core';
 import { HttpClient, HttpHeaders ,HttpParams} from '@angular/common/http';
 import { Products } from '../../_models/products';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { map } from 'rxjs/operators';
 @Injectable(
   {  providedIn: 'root'}
 )
 export class ProductsService {
    // Define API
    apiURL = 'http://localhost:3000';

    constructor(private http: HttpClient) { }
  
    /*========================================
      CRUD Methods for consuming RESTful API
    =========================================*/
  
    // Http Options
    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }  
   
    // HttpClient API get() method => Fetch Products list
    getProducts(): Observable<Products> {
      return this.http.get<Products>(this.apiURL + '/Products')
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
    }
  
    // HttpClient API get() method => Fetch Products
    getProduct(id): Observable<Products> {
      return this.http.get<Products>(this.apiURL + '/Products/' + id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
    }  
  
    // HttpClient API post() method => Create Products
    createProducts(Products): Observable<Products> {
      return this.http.post<Products>(this.apiURL + '/Products', JSON.stringify(Products), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
    }  
  
    // HttpClient API put() method => Update Products
    updateProducts(id, Products): Observable<Products> {
      return this.http.put<Products>(this.apiURL + '/Products/' + id, JSON.stringify(Products), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
    }
  
    // HttpClient API delete() method => Delete Products
    deleteProducts(id){
      return this.http.delete<Products>(this.apiURL + '/Products/' + id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
    }
  
    // Error handling 
    handleError(error) {
       let errorMessage = '';
       if(error.error instanceof ErrorEvent) {
         // Get client-side error
         errorMessage = error.error.message;
       } else {
         // Get server-side error
         errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
       }
       window.alert(errorMessage);
       return throwError(errorMessage);
    }
 }
