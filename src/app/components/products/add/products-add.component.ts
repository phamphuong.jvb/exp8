import { Component, OnInit,Input } from '@angular/core';
import {CdkDragDrop, moveItemInArray,transferArrayItem} from '@angular/cdk/drag-drop';
import {Validators, FormBuilder, FormGroup,FormControl} from '@angular/forms';
import { RouterModule, Routes ,Router,ActivatedRoute} from '@angular/router';

// Services
import { ValidationService } from '../../../services/config/config.service';
import { ProductsService } from '../../../services/products/products.service';
import { routerTransition } from '../../../services/config/config.service';

import { ToastrService } from 'ngx-toastr';
import { HttpClient,HttpEventType } from '@angular/common/http';
import { Http, Headers } from '@angular/http';
@Component({
  selector: 'app-add',
  templateUrl: './products-add.component.html',
  styleUrls: ['./products-add.component.css'],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class ProductsAddComponent implements OnInit {
  id = this.route.snapshot.params['id'];
  constructor(private formBuilder: FormBuilder,private router: Router, private route: ActivatedRoute, private productsService:ProductsService,private toastr: ToastrService) {
  
  }
   productsAddForm : FormGroup;
   productsAddForms : any = {};
  //@Input() productsForm :  { name: '', descripbe: '', type: '' }

  ngOnInit() {
    this.productsService.getProduct(this.id).subscribe((data: {}) => {
      this.productsAddForms = data;
    });
    this.productsAddForm = new FormGroup({
      'name': new FormControl('',  [Validators.required,Validators.minLength(3),Validators.maxLength(50)]),
      'descripbe': new FormControl('',  [Validators.required,Validators.minLength(3),Validators.maxLength(50)]),
      'type': new FormControl('',  [Validators.required])
    });	

   }

  addProduct() {
    this.productsService.createProducts(this.productsAddForm.value).subscribe((data: {}) => {
      this.router.navigate(['/list'])
    })
  }

  // Update employee data
  updateProduct() {
    if(window.confirm('Are you sure, you want to update?')){
      this.productsService.updateProducts(this.id, this.productsAddForm.value).subscribe(data => {
        this.router.navigate(['/list'])
      })
    }
  }

}
