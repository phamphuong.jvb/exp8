import { Component, OnInit } from '@angular/core';
import {Validators, FormBuilder, FormGroup} from '@angular/forms';
import { RouterModule, Routes ,Router,ActivatedRoute} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

// Services
import { ProductsService } from '../../../services/Products/Products.service';
import { routerTransition } from '../../../services/config/config.service';
@Component({
  selector: 'app-details',
  templateUrl: './products-details.component.html',
  styleUrls: ['./products-details.component.css'],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class ProductsDetailsComponent implements OnInit {
  index = this.route.snapshot.params['id'];
  ProductsDetail:any;
  constructor(private router: Router, private route: ActivatedRoute, private ProductsService:ProductsService,private toastr: ToastrService) { 
   
  }

  ngOnInit() {
    this.ProductsService.getProduct(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);
      this.ProductsDetail = data;
    });
  }

}
