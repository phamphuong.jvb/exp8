import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductsService } from 'src/app/services/products/products.service';
import { ToastrService } from 'ngx-toastr';
import { routerTransition } from '../../../services/config/config.service';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

import { Products } from '../../../_models/products';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
@Component({
  selector: 'app-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css'],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class ProductsListComponent implements OnInit {
  Product: any = [];
  displayedColumns: string[] = ['id', 'name', 'descripbe', 'type','submit'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort : MatSort ;

  constructor(private ProductsService:ProductsService,private toastr: ToastrService) { 
  
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.Product.filter = filterValue;
  }

  // Get Products list
  loadProducts() {
    return this.ProductsService.getProducts().subscribe((data: {}) => {
        this.Product = new MatTableDataSource();  
        this.Product.data = data;  
        this.Product.paginator = this.paginator;  
        this.Product.sort = this.sort;  
        console.log(this.Product.data);  
      },  
      error => {  
        console.log('There was an error while retrieving Photos !!!' + error);  
      }); 
   
  }
  ngOnInit() {
    this.loadProducts();
  }
  // Delete Product
  deleteProduct(id) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.ProductsService.deleteProducts(id).subscribe(data => {
        this.loadProducts()
      })
    }
  }  
  // drop(event: CdkDragDrop<string[]>) {
  //   moveItemInArray(this.Product, event.previousIndex, event.currentIndex);
  // }
}