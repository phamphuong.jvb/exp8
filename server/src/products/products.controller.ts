import { Controller,Get,Post,Delete,Param,Body,Query } from '@nestjs/common';
import { ProductsService } from '../../../src/app/services/products/products.service';
import { Products } from '../../../src/app/_models/products';
@Controller('products')
export class ProductsController {
    constructor(private productsService: ProductsService) { }

    @Get()
    async getproducts() {
        const products = await this.productsService.getProducts();
        return products;
    }

    @Get(':productID')
    async getProduct(@Param('productID') productID) {
        const product = await this.productsService.getProduct(productID);
        return product;
    }

    @Post()
    async addProduct(@Body() Products: Products) {
        const product = await this.productsService.createProducts(Products);
        return product;
    }

    @Delete()
    async deleteProduct(@Query() query) {
        const products = await this.productsService.deleteProducts(query.bookID);
        return products;
    }
}
