import { Controller,Get,Post,Delete,Param,Body,Query } from '@nestjs/common';
import { UserService } from '../../../src/app/services/user/user.service';
import { User } from '../../../src/app/_models/user';
import { get } from 'https';
import { async } from 'q';

@Controller('user')
export class UserController {
    constructor(private userService: UserService) { }
    @Get()
    async getUser(email,password){
        const user = await this.userService.login(email,password)
        return user;
    }

}
