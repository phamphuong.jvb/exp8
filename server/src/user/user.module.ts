import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from '../../../src/app/services/user/user.service';

@Module({
  controllers: [UserController],
  providers: [UserService]
})
export class UserModule {}
